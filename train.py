import time
import os
import argparse
import numpy as np
import tqdm

import torch
import torch.nn.functional as F
import torch.optim as optim

from utils import load_data, accuracy
from models import GCN

# Training settings
parser = argparse.ArgumentParser()
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='Disables CUDA training.')
parser.add_argument('--fastmode', action='store_true', default=False,
                    help='Validate during training pass.')
parser.add_argument('--seed', type=int, default=42, help='Random seed.')
parser.add_argument('--epochs', type=int, default=10000,
                    help='Number of epochs to train.')
parser.add_argument('--lr', type=float, default=0.001,
                    help='Initial learning rate.')
parser.add_argument('--weight_decay', type=float, default=5e-4,
                    help='Weight decay (L2 loss on parameters).')
parser.add_argument('--hidden', type=int, default=16,
                    help='Number of hidden units.')
parser.add_argument('--dropout', type=float, default=0.5,
                    help='Dropout rate (1 - keep probability).')
parser.add_argument('--patience', default=400, type=int, help=' Time to wait without improvement.')
parser.add_argument('--minc', default=5, type=int, help='minimum examples for the labels.')
parser.add_argument('--dim', default=128, type=int, help='dimension of embedding space (features)')
parser.add_argument('--init_embed', default="embed", type=str, help='type of embedding (random|resnet|embed). Embed is for both node2vec and transe')
parser.add_argument('--graph_embs', default="../context-art-classification/Data/semart-artgraph-node2vec.model", type=str)
parser.add_argument('--verbose', action='store_true', default=False,
                    help='verbose.')
parser.add_argument('--att', default='time', type=str, help='Attribute classifier (type | school | time | author) .')
parser.add_argument('--ct', default='', type=str, help='Corrupted Attribute classifier (type | school | time | author) .')
parser.add_argument('--device_id', default="0", type=str, help='device_id to use .')

args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

if args.att=="author":
    args.patience = 1000

np.random.seed(args.seed)
torch.manual_seed(args.seed)
if args.cuda:
    if args.verbose:
        print("cuda on")
    # torch.cuda.device(args.device_id)
    os.environ["CUDA_VISIBLE_DEVICES"] = args.device_id
    torch.cuda.manual_seed(args.seed)
else:
    if args.verbose:
        print("cuda off")

# Load data
adj, features, labels, idx_train, idx_val, idx_test = load_data("Semart", args)

# Model and optimizer
model = GCN(nfeat=features.shape[1],
            nhid=args.hidden,
            nclass=labels.max().item() + 1,
            dropout=args.dropout)
# optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=0.9)
optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)

if args.cuda:
    model.cuda()
    features = features.cuda()
    adj = adj.cuda()
    labels = labels.cuda()
    idx_train = idx_train.cuda()
    idx_val = idx_val.cuda()
    idx_test = idx_test.cuda()


def train_epoch(epoch):
    t = time.time()
    # adj[idx_train] = adj[idx_train]/len(adj)
    model.train()
    optimizer.zero_grad()
    output = model(features, adj)
    loss_train = F.nll_loss(output[idx_train], labels[idx_train])
    acc_train = accuracy(output[idx_train], labels[idx_train])
    loss_train.backward()
    optimizer.step()

    if not args.fastmode and args.verbose and epoch%10==0:
        # Evaluate validation set performance separately,
        # deactivates dropout during validation run.
        model.eval()
        output = model(features, adj)
        loss_test = F.nll_loss(output[idx_test], labels[idx_test])
        acc_test = accuracy(output[idx_test], labels[idx_test])
        print("Test set results:",
            "loss= {:.4f}".format(loss_test.item()),
            "accuracy= {:.4f}".format(acc_test.item()))


    loss_val = F.nll_loss(output[idx_val], labels[idx_val])
    acc_val = accuracy(output[idx_val], labels[idx_val])
    if args.verbose:
        if epoch%10==0:
            print('Epoch: {:04d}'.format(epoch+1),
            'loss_train: {:.4f}'.format(loss_train.item()),
            'acc_train: {:.4f}'.format(acc_train.item()),
            'loss_val: {:.4f}'.format(loss_val.item()),
            'acc_val: {:.4f}'.format(acc_val.item()),
            'time: {:.4f}s'.format(time.time() - t))
    return acc_val


def test():
    model.eval()
    output = model(features, adj)
    loss_test = F.nll_loss(output[idx_test], labels[idx_test])
    acc_test = accuracy(output[idx_test], labels[idx_test])
    if args.verbose:
        print("Test set results:",
          "loss= {:.4f}".format(loss_test.item()),
          "accuracy= {:.4f}".format(acc_test.item()))
    else:
        print("{},{:.4f}".format(args.att,acc_test.item()))



def train():
    t_total = time.time()
    pat_track=0
    best_val = float(0)
    bar = tqdm.tqdm(total = args.epochs)
    for epoch in range(args.epochs):
        if not args.verbose:
            bar.update(1)
        acc_val = train_epoch(epoch)
        if acc_val <= best_val:
            pat_track += 1
        else:
            pat_track = 0
        if pat_track >= args.patience:
            break
        best_val = max(acc_val, best_val)
    if args.verbose:
        print()
        print("Optimization Finished!")
        print("Total time elapsed: {:.4f}s".format(time.time() - t_total))

# Train model
train()
# Testing
test()

