#!/bin/bash
#OAR -n GCN for semart dataset node2vec

#OAR -l /nodes=1/gpu_device=1,walltime=10:00:00
#OAR -p virt='YES'
#OAR -O igrida.%jobid%.output
#OAR -E igrida.%jobid%.output

#. /etc/profile.d/modules.sh
echo "att,acc"
for i in 1 2 3 4 5 6 7 8 9 10
do
    #echo run:$i
    for att in "type" "school" "time" "author" 
    do
        #echo $1
        python3 train.py  --att $att  --ct  $1 --device_id 1 --patience 400
        #wait
    done
    #exit
    #echo run:$i
    # python train_mgcn.py
    # python train.py  --att author --minc 5
done
