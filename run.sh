#!/bin/bash
#OAR -n GCN for semart dataset node2vec

#OAR -l /nodes=1/gpu_device=1,walltime=10:00:00
#OAR -O igrida.%jobid%.output
#OAR -E igrida.%jobid%.output

#. /etc/profile.d/modules.sh
for corrupted in "SCHOOL" "TIME" "AUTHOR" "TYPESCHOOLTIME" "TYPESCHOOLAUTHOR" "TYPEAUTHORTIME" "SCHOOLAUTHORTIME" "TYPETIME" "TYPESCHOOL" "TYPEAUTHOR" "TIMESCHOOL" "TIMEAUTHOR"#
do
    echo $corrupted
    echo "att,acc"
    for att in "type" "school" "time" "author" 
    do
        #echo $1
        python train.py  --att $att  --ct  $corrupted --device_id 0
        #wait
    done
done