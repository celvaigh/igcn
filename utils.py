import sys

import networkx as nx
import pandas as pd
import ast
import numpy as np
import tqdm , os
import random
from gensim.models import Word2Vec, KeyedVectors

import scipy.sparse as sp
import torch.nn as nn
from torchvision import models
import torch
from PIL import Image
from torchvision import transforms
import heapq
import collections
import random
#oarsub -I -l /gpu_device=1,walltime=6:00:00

def random_prediction(path):
    filename=os.path.join(path, "semart_pred.csv")
    df = pd.read_csv(filename, delimiter='\t',  encoding='Cp1252')
    authors = df['AUTHOR'].fillna('')
    types = df['TYPE'].fillna('')
    schools = df['SCHOOL'].fillna('')
    timeframes = df['TIMEFRAME'].fillna('')
    atts = ["AUTHOR", "TYPE", "SCHOOL", "TIME"]
    data = [authors, types, schools, timeframes]
    for i in range(len(data)):
        labels = data[i]
        df['RANDOM_{}'.format(atts[i])]=[random.choice(list(set(labels))) for _ in range(len(labels))]
    filename=os.path.join(path, "semart_pred.csv")
    df.to_csv(filename,sep='\t', index=False)

def create_grapg(G, path="../SemArt",filename="semart_train_kw.csv", algo="", delete_nodes=[], corrupted=[]):
    # Load data
    # G = nx.Graph()
    init_nodes=G.number_of_nodes()
    df = pd.read_csv(os.path.join(path, filename), delimiter='\t',  encoding='Cp1252')
    num_paint = len(df)
    # print('SemArt dataset with %d training images' % num_paint)
    # Extract attributes
    if len(algo):
        timekey = 'TIME'
    else:
        timekey = 'TIMEFRAME'
    paintings = df['IMAGE_FILE']
    

    if "AUTHOR" in corrupted:
        authors = df['RANDOM_AUTHOR'].fillna('')
    else:
        authors = df['{}AUTHOR'.format(algo)].fillna('')
    if "SCHOOL" in corrupted:
        schools = df['RANDOM_SCHOOL'].fillna('')
    else:
        schools = df['{}SCHOOL'.format(algo)].fillna('')
    if "TYPE" in corrupted:
        types = df['RANDOM_TYPE'].fillna('')
    else:
        types = df['{}TYPE'.format(algo)].fillna('')
    if "TIME" in corrupted:
        timeframes = df['RANDOM_TIME'].fillna('')
    else:
        timeframes = df['{}'.format(algo)+timekey].fillna('')
    
    # keywords = df['KEYWORDS_NGRAMS'].fillna('')
    # technique = df['TECHNIQUE'].fillna('')
    # keywords = ast.literal_eval(keywords)
    # Remove unknown authors
    authors = authors.str.replace('UNKNOWN(.*)', '')
    
    # Split technique into support and material
    # material = technique.str.split(',').str[0].fillna('')
    # support = technique.str.split(',').str[1].fillna('')
      
    # Edges between paintings and their attributes
    for index in range(num_paint):

        # Print info
        # if index % 500 == 0:
        #     print('Painting %d / %d' % (index, num_paint))
        if paintings[index] in delete_nodes:continue
        #if ".jpg" not in paintings[index]:print(paintings[index])
        # Add edge timeframe
        if timeframes[index]:
            G.add_edge(paintings[index], timeframes[index])

        # Add edge author & school
        if authors[index]:
            G.add_edge(paintings[index], authors[index])
            if schools[index]:
                G.add_edge(authors[index], schools[index])

        if schools[index]:
            G.add_edge(paintings[index], schools[index])

        # Add edge type
        if types[index]:
            G.add_edge(paintings[index], types[index])

        # Add edge material
        # if material[index]:
        #     G.add_edge(paintings[index], material[index])

        # Add edge support
        # if support[index]:
        #     G.add_edge(paintings[index], support[index])

        # Add edges keywords
        # this_kws = ast.literal_eval(keywords[index])
        # for kw in this_kws:
        #     G.add_edge(paintings[index], kw)
    nodes=list(G.nodes())
    return [nodes.index(im) for im in paintings if im not in delete_nodes]
 
transformer = transforms.Compose([
        transforms.Resize(256),                             # rescale the image keeping the original aspect ratio
        transforms.ToTensor(),                              # to pytorch tensor
        transforms.Normalize(mean=[0.485, 0.456, 0.406, ],  # ImageNet mean substraction
                             std=[0.229, 0.224, 0.225])
    ])
oov = 0
def load_data(path, args, labels_only=False):

    
    # print("train images:{}, test images:{} and val images:{}".format(len(idx_train),len(idx_test),len(idx_val)))
    # print('Created graph with %d nodes and %d edges' % (G.number_of_nodes(), G.number_of_edges()))
    # print('Writing...')
    # nx.write_edgelist(G, os.path.join(path, "semart_all"), delimiter='\t',data=False)
    dfs=[]
    semart="../../SemArt/SemArt"
    random_prediction(semart)
    for filename in ["semart_train.csv","semart_val.csv"]:#"semart_test.csv"]:
        textfile = os.path.join(semart, filename)
        dfs += [pd.read_csv(textfile, delimiter='\t', encoding='Cp1252')]
    df=pd.concat(dfs)
    dftest = pd.read_csv(os.path.join(semart, "semart_pred.csv"), delimiter='\t', encoding='Cp1252')
    att_name = args.att
    algo="NODE2VEC_{}".format(att_name.upper()) 
    if att_name == 'type':
        att = list(df['TYPE'])
    elif att_name == 'school':
        att = list(df['SCHOOL'])
    elif att_name == 'time':
        att = list(df['TIMEFRAME'])
    elif att_name == 'author':
        att = list(df['AUTHOR'])
        # print(len(set(att)))
        author_counter = collections.Counter(att)
        #top_authors = filter(lambda x: author_counter[x]>5, author_counter)
        top_att = []
        #print(len([x for x in author_counter if author_counter[x]==1])); exit()
        for a in att :
            if author_counter[a]>= 5 :
                top_att +=[a]
            else:
                top_att +=["UNK"]
        att = top_att
    att += list(dftest[algo])
    # print(list(dftest[algo])[:10])

    imageurls = list(df['IMAGE_FILE']) + list(dftest['IMAGE_FILE'])
    # features = get_resnet_features(imageurls)
    dic={imageurls[i]:i for i in tqdm.tqdm(range(len(imageurls)))}
    _delete_nodes = [imageurls[i] for i in range(len(att)) if att[i]=="UNK"]

    #random_prediction(semart)
    # Create graph
    G = nx.Graph()
    idx_train, idx_val  = [create_grapg(G,path=semart, filename="semart_{}_kw.csv".format(x), delete_nodes=_delete_nodes) for x in ["train","val"]]
    idx_test = create_grapg(G,filename="semart_pred.csv", path=semart, algo="NODE2VEC_", delete_nodes=_delete_nodes, corrupted=args.ct)
      
    features = []
    slabels = []
    nodes = G.nodes()
    for x in nodes:
        try:
            # imges got labels
            slabels += [att[dic[x]]] 
        except: 
            # non imges are labels, won't be used, fake labels to have the correct shape
            slabels +=  [x] 

    # print(slabels)
    
    labels = encode_onehot(slabels)
    # print("labels : {}".format(labels.shape))
    labels = torch.LongTensor(np.where(labels)[1])
    if labels_only:
        return labels

    features = get_embedding(args, imageurls, nodes)
    # print("features : {}".format(features.shape))
    features = sp.csr_matrix(np.array(features))
    # features = normalize(features)
    features = torch.FloatTensor(np.array(features.todense()))

    # build symmetric adjacency matrix
    adj = nx.adjacency_matrix(G)
    adj = adj + adj.T.multiply(adj.T > adj) - adj.multiply(adj.T > adj)
    adj = normalize(adj + sp.eye(adj.shape[0]))
    # adj = adj + sp.eye(adj.shape[0])


    # print("adj : {}".format(adj.shape))
    adj = sparse_mx_to_torch_sparse_tensor(adj)
    
    idx_train = torch.LongTensor(idx_train)
    idx_val = torch.LongTensor(idx_val)
    idx_test = torch.LongTensor(idx_test)
    # print("train : {}, val:{}, test:{}".format(idx_train.shape, idx_val.shape, idx_test.shape))

    return adj, features, labels, idx_train, idx_val, idx_test

def get_embedding(args, imageurls, nodes):
    if args.init_embed=="random":
        return get_random_features(len(nodes),args.dim)
    elif args.init_embed=="resnet":
        return get_resnet_features(imageurls)
    elif args.init_embed=="embed":
        return get_embedding_features(args.graph_embs, nodes, dim=args.dim)
    else:
        print("init module unk")
        exit()
def get_resnet_features(imageurls):
    
    ifeatures=[]
    imagefolder = os.path.join("../SemArt","Images/")
    try:
        ifeatures = np.load("Semart/features.npy")
    except Exception as e:
        # print(e) 
        resnet = models.resnet50(pretrained=True)
        extractor = nn.Sequential(*list(resnet.children())[:-1])
        for i in tqdm.tqdm(range(len(imageurls))):
            imagepath = imagefolder + imageurls[i]
            image = transformer(Image.open(imagepath).convert('RGB')).unsqueeze(0)
            resnet_f=extractor(image)
            x=resnet_f.squeeze(0).detach().numpy()
            ifeatures+=[np.array([t[0][0] for t in x])]
        ifeatures = np.array(ifeatures)
        np.save("Semart/features.npy", ifeatures)
    return ifeatures

def get_random_features(n_nodes,dim=128):
    return  np.array([np.array([random.random() for i in range(dim)]) for n in range(n_nodes)])

def get_embedding_features(modelname, nodes, dim=128):
    
    def get_vector(im,dim):
        try: return graphEm[im]
        except: return np.array([random.random() for i in range(dim)])

    if "node2vec" in modelname:
        graphEm = Word2Vec.load(modelname)
    else:
        graphEm = KeyedVectors.load_word2vec_format(modelname , binary=False,unicode_errors='ignore')
    return  np.array([get_vector(n, dim) for n in nodes])
    
    

def sparse_mx_to_torch_sparse_tensor(sparse_mx):
    """Convert a scipy sparse matrix to a torch sparse tensor."""
    sparse_mx = sparse_mx.tocoo().astype(np.float32)
    indices = torch.from_numpy(
        np.vstack((sparse_mx.row, sparse_mx.col)).astype(np.int64))
    values = torch.from_numpy(sparse_mx.data)
    shape = torch.Size(sparse_mx.shape)
    return torch.sparse.FloatTensor(indices, values, shape)

def normalize(mx):
    """Row-normalize sparse matrix"""
    rowsum = np.array(mx.sum(1))
    r_inv = np.power(rowsum, -1).flatten()
    r_inv[np.isinf(r_inv)] = 0.
    r_mat_inv = sp.diags(r_inv)
    mx = r_mat_inv.dot(mx)
    return mx

def accuracy(output, labels):
    preds = output.max(1)[1].type_as(labels)
    correct = preds.eq(labels).double()
    correct = correct.sum()
    return correct / len(labels)

def encode_onehot(labels):
    classes = set(labels)
    classes_dict = {c: np.identity(len(classes))[i, :] for i, c in
                    enumerate(classes)}
    labels_onehot = np.array(list(map(classes_dict.get, labels)),
                             dtype=np.int32)
    return labels_onehot

def stats(filename):
    # with open(filename) as f:
    #     data=[d.split(":") for d in f.read().split("\n")[1:]]
    df = pd.read_csv(filename, delimiter=',')
    
    att = df["att"]
    acc = df["acc"]
    dic={}
    #tqdm.tqdm(
    for i in range(len(att)):
        try: dic[att[i]] += [float(acc[i])]
        except: dic[att[i]] = [float(acc[i])]
    #print("att,acc,std")
    for key in dic:
        print("{},{},{}".format(key, np.mean(dic[key]), np.std(dic[key])))

def data_analysis(filename):

    textfile = os.path.join("../SemArt", filename)
    df = pd.read_csv(textfile, delimiter='\t', encoding='Cp1252')
    if "train" in filename:
        target = "train"
    elif "test" in filename:
        target = "test"
    else:
        target = "val"
    print("Processing {} ".format(target))
    for att_name in ["TYPE", "SCHOOL", "TIMEFRAME", "AUTHOR"]:
        att = list(df[att_name])
        counter = collections.Counter(att)
        print("Att:{}, Labels:{}".format(att_name, len(counter)))
        for label in counter:
            print("{} : {}".format(label.encode('utf-8'), counter[label]))

       
if __name__ == "__main__":
    # make_data("Semart")
    if len(sys.argv) == 3:
        data_analysis(sys.argv[1])
    else:
        stats(sys.argv[1])
