## GCN Embeddings for Art Classification

### Setup

1. Download dataset from [here](http://noagarciad.com/SemArt/).

2. Clone the repository: 
    
    `git clone https://gitlab.inria.fr/celvaigh/igcn.git`

3. Install dependencies:
    - Python 2.7
    - pytorch (`conda install pytorch=0.4.1 cuda90 -c pytorch`) 
    - torchvision (`conda install torchvision`)
    - pandas (`conda install -c anaconda pandas`)
    - gensim (`conda install -c anaconda gensim`)

### Train and Test

- GCN classifier run:
    
    `python train.py`

- GCN multi-classifier run:
    
    `python train_mgcn.py`

### Results
 
Classification results on SemArt:

| Model        | Type           | School  |    Timeframe    | Author |
| ------------- |:-------------:| -----:|---------:|--------:|
| VGG16 pre-trained | 0.706 | 0.502 | 0.418 | 0.482 |
| ResNet50 pre-trained | 0.726 | 0.557 | 0.456 | 0.500 | 
| ResNet152 pre-trained | 0.740 | 0.540 | 0.454 | 0.489 |
| VGG16 fine-tuned | 0.768 | 0.616 | 0.559 | 0.520 |
| ResNet50 fine-tuned | 0.765 | 0.655 | 0.604 | 0.515 |
| ResNet152 fine-tuned | 0.790 | 0.653 | 0.598 | 0.573 |
| ResNet50+Attributes | 0.785 | 0.667 | 0.599 | 0.561 |
| ResNet50+Captions | 0.799 | 0.649 | 0.598 | 0.607 |
| MTL context-aware | 0.791 | 0.691 | 0.632 | 0.603 |
| KGM context-aware | 0.815 | 0.671 | 0.613 | 0.615 |  
| Joint Model(JIGE) | 0.820 | 0.682 | 0.620 | **0.624** |  
|   Random + GCN  | 0.260 | 0.356 | 0.134 | 0.022 |  
|   Node2vec + GCN  | 0.934  | 0.928 | 0.947 | 0.552 |  
|   TransE + GCN  | 0.954 | 0.930 | 0.943 | 0.538|
|   Random + MGCN  | 0.397 | 0.401 | 0.149 | 0.026 |  
|   Node2vec + MGCN  | 0.954 | 0.922 | 0.959 | 0.538 |  
|   TransE + MGCN  | **0.991** | **0.953** | **0.969** | 0.501 |  
