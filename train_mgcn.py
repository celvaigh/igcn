import time
import argparse
import numpy as np
import tqdm

import torch
import torch.nn.functional as F
import torch.optim as optim

from utils import load_data, accuracy
from models import MGCN

# Training settings
parser = argparse.ArgumentParser()
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='Disables CUDA training.')
parser.add_argument('--fastmode', action='store_true', default=False,
                    help='Validate during training pass.')
parser.add_argument('--seed', type=int, default=42, help='Random seed.')
parser.add_argument('--epochs', type=int, default=5000,
                    help='Number of epochs to train.')
parser.add_argument('--lr', type=float, default=0.001,
                    help='Initial learning rate.')
parser.add_argument('--weight_decay', type=float, default=5e-4,
                    help='Weight decay (L2 loss on parameters).')
parser.add_argument('--hidden', type=int, default=16,
                    help='Number of hidden units.')
parser.add_argument('--dropout', type=float, default=0.5,
                    help='Dropout rate (1 - keep probability).')
parser.add_argument('--patience', default=200, type=int, help=' Time to wait without improvement.')
parser.add_argument('--dim', default=128, type=int, help='dimension of embedding space (features)')
parser.add_argument('--init_embed', default="embed", type=str, help='type of embedding (random|resnet|embed). Embed is for both node2vec and transe')
parser.add_argument('--graph_embs', default="../context-art-classification/Data/semart-artgraph-node2vec.model", type=str)
parser.add_argument('--verbose', action='store_true', default=False,
                    help='verbose.')
parser.add_argument('--att', default='type', type=str, help='Attribute classifier (type | school | time | author) .')

args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

np.random.seed(args.seed)
torch.manual_seed(args.seed)
if args.cuda:
    if args.verbose:
        print("cuda on")
    torch.cuda.manual_seed(args.seed)
else:
    if args.verbose:
        print("cuda off")

# Load data
adj, features, labels, idx_train, idx_val, idx_test = load_data("Semart", args)

labels=[labels]
for att in ["school", "time", "author"]:
    args.att = att
    labels += [ load_data("Semart", args, labels_only=True)]
nclass = [l.max().item() + 1 for l in labels]
# Model and optimizer
model = MGCN(nfeat=features.shape[1],
            nhid=args.hidden,
            nclass=nclass,
            dropout=args.dropout)

# optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=0.9)
optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)

if args.cuda:
    model.cuda()
    features = features.cuda()
    adj = adj.cuda()
    labels = [l.cuda() for l in labels]
    idx_train = idx_train.cuda()
    idx_val = idx_val.cuda()
    idx_test = idx_test.cuda()


def train_epoch(epoch):
    t = time.time()
    # adj[idx_train] = adj[idx_train]/len(adj)
    model.train()
    optimizer.zero_grad()
    output = model(features, adj)
    
    loss_train = sum([F.nll_loss(output[l][idx_train], labels[l][idx_train])for l in range(len(labels))])
    acc_train = [accuracy(output[l][idx_train], labels[l][idx_train])for l in range(len(labels))]
    loss_train.backward()
    optimizer.step()


    loss_val = sum([F.nll_loss(output[l][idx_val], labels[l][idx_val])for l in range(len(labels))])
    acc_val = [accuracy(output[l][idx_val], labels[l][idx_val])for l in range(len(labels))]
    if args.verbose:
        
        if epoch%10==0:
            atti = 0
            for att in ["type", "school", "time", "author"]:
                print('Epoch: {:04d}'.format(epoch+1),
                'Att:{}'.format(att),
                'loss_train: {:.4f}'.format(loss_train.item()),
                'acc_train: {:.4f}'.format(acc_train[atti].item()),
                'loss_val: {:.4f}'.format(loss_val.item()),
                'acc_val: {:.4f}'.format(acc_val[atti].item()),
                'time: {:.4f}s'.format(time.time() - t))
                atti += 1
            print()
    return acc_val


def test():
    model.eval()
    output = model(features, adj)
    loss_test = sum([F.nll_loss(output[l][idx_test], labels[l][idx_test]) for l in range(len(labels))])
    acc_test  = [ accuracy(output[l][idx_test], labels[l][idx_test]) for l in range(len(labels))]
    if args.verbose:
        atti = 0
        for att in ["type", "school", "time", "author"]:
            print("Test set results:",
            "Att:{}".format(att),
            "loss= {:.4f}".format(loss_test.item()),
            "accuracy= {:.4f}".format(acc_test[atti].item()))
            atti += 1
    else:
        atti = 0
        for att in ["type", "school", "time", "author"]:
            print("{},{:.4f}".format(att,acc_test[atti].item()))
            atti += 1



def train():
    t_total = time.time()
    pat_track=0
    best_val = float(0)
    for epoch in tqdm.tqdm(range(args.epochs)):
        acc_val = train_epoch(epoch)
        # if acc_val <= best_val:
        #     pat_track += 1
        # else:
        #     pat_track = 0
        # if pat_track >= args.patience:
        #     break
        # best_val = max(acc_val, best_val)
    if args.verbose:
        print()
        print("Optimization Finished!")
        print("Total time elapsed: {:.4f}s".format(time.time() - t_total))

# Train model
train()
# Testing
test()

