#!/bin/bash
#OAR -n GCN for semart dataset transe

#OAR -l /nodes=1/gpu_device=1,walltime=06:00:00
#OAR -p virt='YES'
#OAR -O igrida.%jobid%.output
#OAR -E igrida.%jobid%.output

echo "att,acc"
for i in 1 2 3 4 5 6 7 8 9 10
do
    #echo run:$i
    for att in "type" "school" "time" "author" 
    do
        #echo $att
        python train.py  --att $att  --graph_embs ../graph-embedding/transe128/semart-artgraph-transe.model.txt
        wait
    done
    # python train_mgcn.py  --graph_embs ../graph-embedding/transe128/semart-artgraph-transe.model.txt
    # python train.py  --att author  --graph_embs ../graph-embedding/transe128/semart-artgraph-transe.model.txt --minc 5
done